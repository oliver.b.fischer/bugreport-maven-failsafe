package net.sweblog.failsaferunalways.implicit;

import org.testng.Assert;
import org.testng.annotations.Test;

@Test
public class ExampleIT
{
    public void iWillFAilIfFailsafeWillBeExecuted() {
        Assert.fail("I will fail if Maven Failsafe will be executed.");
    }
}
