package net.sweblog.failsaferunalways.unittesting;

import org.testng.Assert;
import org.testng.annotations.Test;

@Test
public class ExampleTest
{
    public void iWillFAilIfSurefireWillBeExecuted() {
        Assert.fail("I will fail if Maven Surefire will be executed.");
    }
}
